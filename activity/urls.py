from django.urls import path
from . import views

app_name = "activity"

urlpatterns = [
    path('activity/', views.activity, name='activity'),
    path('result/',views.result, name = 'result'),
]
