from django.db import models

# Create your models here.
class Individual(models.Model):
    age = models.CharField(max_length=3)
    gender = models.CharField(max_length=10, default= "boy")
    weight = models.CharField(max_length=3, default="55")
    height = models.CharField(max_length=3, default= "159")
