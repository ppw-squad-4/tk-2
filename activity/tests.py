from django.test import TestCase,Client
from django.urls import resolve, reverse
from . import views
from .views import *
from .models import *
import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
class uniTest(TestCase):
    def test_ada_url(self):
        response = Client().get("/activity/")
        self.assertEqual(response.status_code,200)

    def test_ada_fungsi(self):
        response = Client().get('/activity/')
        target = resolve('/activity/')
        self.assertContains(response,'rendah')
        self.assertTrue(target.func, activity)

    def test_ada_template(self):
        response = Client().get("/activity/")
        self.assertTemplateUsed(response,"activity.html")

    def test_ada_button(self):
        response = Client().get("/activity/")
        content = response.content.decode("utf8")
        self.assertIn("<button",content)

    def buat_model(self, age="12",gender="boi",weight="74",height="175"):
        return Individual.objects.create(age = age,gender=gender,weight=weight,height=height)

    def test_buat_model(self):
        w = self.buat_model()
        self.assertEqual(1,Individual.objects.all().count())
        self.assertNotEqual(0,Individual.objects.all().count())

    def test_ada_form(self):
        response = Client().get("/activity/")
        content = response.content.decode("utf8")
        self.assertIn("<form",content)

    def test_client_post(self):
        # response = Client().post('/age/activity',{'age_va;':'15'})
        response = Client().post('',{'gender':'boi','age_vale':'12','weight_val':'74',
        'height_val':'174', 'activeness':'sedang','food_val':'ayam','sleep_val':'6','energy':'2200'})


        self.assertEqual(response.status_code,200)
    def test_ada_fungsi2(self):
        response = Client().get('/result/')
        target = resolve('/result/')
        # self.assertContains(response,context)
        self.assertTrue(target.func, result)



    # def test_story_form(self):
    #     form = (data={'gender':'boi','age_vale':'12','weight_val':'74',
    #     'height_val':'174', 'activeness':'sedang','food_val':'ayam','sleep_val':'6','energy':'2200'})
    #     self.assertTrue(form.is_valid())
        # t = form.cleaned_data['title']
        # self.assertEqual(t,'judul')
        # t1 = form.cleaned_data['text']
        # self.assertEqual(t1,'ini ceritanya')


# class functionalTests(unittest.TestCase):
#
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.browser = webdriver.Chrome('./chromedriver',chrome_options = chrome_options)
#
#     def tearDown(self):
#         self.browser.quit()
#
#     def test_masukin_form_coba_coba(self):
#         self.browser.get("http://127.0.0.1:8000/activity")
#         time.sleep(5)
#
#         search_box = self.browser.find_element_by_id("age")
#         search_box.send_keys("17")
#
#         search_box = self.browser.find_element_by_id("btn.next-button")
#         search_box.submit()
#
#         # search_box.submit()
#
#         time.sleep(2)
#         self.assertIn("Coba Coba", self.browser.page_source)
