# Generated by Django 2.2.5 on 2019-12-06 09:57

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('activity', '0002_auto_20191206_1655'),
    ]

    operations = [
        migrations.RenameField(
            model_name='individual',
            old_name='weigth',
            new_name='weight',
        ),
    ]
