from . import models
from django import forms


class TestimoniForm(forms.Form):
    rating = forms.IntegerField(min_value=1, max_value=5,
                                widget=forms.NumberInput(attrs={'required' : 'True',
                                                                'id':'rating'}))
    pesan = forms.CharField(
            widget=forms.Textarea(attrs={'required' : 'True', 'class':'form-control',
                                         'label' : 'Your testimoni', 'id':'pesan'})
							)
    
    
    
