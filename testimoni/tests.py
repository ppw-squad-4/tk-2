from django.test import TestCase, Client
from django.urls import resolve, reverse
from .models import Testimoni
from django.utils import timezone
from django.contrib.auth.models import User
from . import views
from django.contrib.auth import authenticate, login, logout

class TestimoniTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = User.objects.create_user('valnicolas', 'nicolas@gmail.com', 'valentinus')
        cls.user.full_name = 'Nicolas Henry'
        cls.user.save()

    def test_status_url_exits(self):
        response = Client().get('/testi/')
        self.assertEqual(response.status_code,200);

    def test_template_used(self):
        response = Client().get('/testi/')
        self.assertTemplateUsed(response, 'index.html')

    def test_testimoni_model(self):
        new_testimoni = Testimoni.objects.create(nama = 'Nicolas', pesan = "halo anjing", rating = 4, date = timezone.now())
        counting_all_testimoni = Testimoni.objects.all().count()
        self.assertEqual(counting_all_testimoni, 1)

    def test_username_valid(self):
        form_data = {'username':'valnicolas', 'password':'valentinus'}
        user = authenticate(data=form_data)
        self.assertTrue(user is None);

    
    # def test_can_save_a_POST_request(self):
    #     form_data = {'username':'valnicolas', 'password':'valentinus'}
    #     user = authenticate(data=form_data)
    #     response = self.client.post(reverse('testimoni:index'), 
    #         data={'pesan':'auinwdiad', 'rating': 4})
    #     # response = self.client.post('/testi/testjson_testi/',
    #     #                             data={'nama':'nicolas' ,'pesan' : 'halo',
    #     #                                 'rating' : 4})
    #     counting = Testimoni.objects.all().count()
    #     self.assertEqual(counting,1)

    # def test_not_POST_request(self):
    #     response = self.client.get(
    #         reverse('testimoni:add_testi'), data={
    #             'rating': 1,
    #             'pesan' : 'adawdads',
    #             }
    #         )
    #     response = self.client.get(reverse('testimoni:index'))
    #     counting = Status.objects.all.count()
    #     self.assertNotEqual(counting,1)
    #     self.assertNotIn(self.client.pesan, html)
    
    def test_user_belom_LogIn(self):
        response = self.client.post(reverse('testimoni:json_testi'), 
            data={'username':'idanid','password':'auinwdiad'})
        self.assertEqual(response.status_code,403)
        

# Create your tests here.
