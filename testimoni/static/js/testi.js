// $(function() {
//   // this initializes the dialog (and uses some common options that I do)
// 	$("#dialog").dialog({autoOpen : false, modal : true, show : "blind", hide : "blind"});

//   // next add the onclick handler
// 	$("#contactUs").click(function() {
// 		$("#dialog").dialog("open");
// 		return false;
// 	});
// });

$( function() {
    $("#dialog").dialog({autoOpen : true, modal : true, show : "blind", hide : "blind"});
});

$(function() {
    $(".testibox").mouseover(function() {
        $(this).css('background-color', '#546e7a'),
        $(this).css('color', '#ffab40');
        
    }).mouseout(function() {
    	$(this).css('background-color', '#ffffff'),
    	$(this).css('color', '');
    });
});

$(document).ready(function(){
  $("#show").click(function(){
    $(".funfactpanjang").show();
  });
  $("#hide").click(function(){
    $(".funfactpanjang").hide();
  });
});

var csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});


$(document).ready(
    $('#formtesti').submit(function (event) {
        event.preventDefault();
        const rating = $('#rating').val();
        const pesan = $('#pesan').val();
        $.ajax({
            method: 'POST',
            url: "/testi/testjson_testi/",
            data:{rating : rating, 
            	pesan: pesan, 
            	csrfmiddlewaretoken: csrftoken},
            success: function (result) {
                let testirow = $('.hasiltesti'); 
                // bookRow.empty();
                testirow.append(
                    "<div class='card mb-3 testibox' style='max-width: 18rem;'>"+
                	"<div class='card-header'><strong>" + result.nama + "</strong></div>" +
                	"<div class='card-body'>" + 
                	"<h5 class='card-title'>" + result.rating + "/ 5</h5>"+
                	"<p class='card-text'>"+ result.pesan+ "</p>"+
                	"<div>" + "<div>"
                );
                $("#rating").val("");
                $("#pesan").val("");
            },
            statusCode:{
            	403: function(){
            		alert('Please login first!');
            	}
            }

        });
    })
);





// $(function() {
//     $(".testibox").hover(
//     function() {
//         $(this).css('background-color', 'red')
//     }, function() {
//         $(this).css('background-color', '')
//     });
// });


