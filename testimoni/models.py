from django.db import models
from django.db import models
from django.utils import timezone
from datetime import datetime, date
from django.core.validators import MaxValueValidator, MinValueValidator
from django.contrib.auth.models import User

class Testimoni(models.Model):
    nama = models.CharField(max_length=100, default="*No Name*")
    rating = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(5)])
    pesan = models.TextField()
    date = models.DateTimeField(auto_now= True, blank=True)


# Create your models here.
