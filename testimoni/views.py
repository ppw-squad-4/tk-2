from django.shortcuts import render, redirect
from .models import Testimoni
from .forms import TestimoniForm
from django.http import JsonResponse, HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout


def index(request):
    hasil = Testimoni.objects.all()
    form = TestimoniForm()
    return render(request, 'index.html', {"form": form,"testi":hasil})

def json_testi(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = TestimoniForm(request.POST)
            if form.is_valid():
                nama = request.user.get_full_name()
                rating = request.POST['rating']
                pesan = request.POST['pesan']
                hasil = Testimoni.objects.create(nama= nama, 
                                                rating = form.data['rating'],
                                                pesan = form.data['pesan'])
                hasil.save()
                json = JsonResponse({'nama':nama, 'rating':rating,'pesan':pesan}, safe=False)
                return json
    message = 'login dulu'
    response = JsonResponse({'status':'false','message':message})
    response.status_code = 403
    return response;
            
    

# Create your views here.
