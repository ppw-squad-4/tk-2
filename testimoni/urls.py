from django.urls import path
from django.conf.urls import url
from . import views

app_name = 'testimoni'

urlpatterns = [
    path('', views.index, name='index'),
    #path('add_testi/', views.add_testi, name="add_testi")
    path('testjson_testi/', views.json_testi, name="json_testi")
    # dilanjutkan ...
]
