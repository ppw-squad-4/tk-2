$(document).ready(function() {
    //Untuk register page
    $("#fname").click(function() {
        $("#fname").focus(function() {
            $(this).css({"background-color":"aqua","border-radius":"20%"});
        });
    });
    $("#lname").click(function() {
        $("#lname").focus(function() {
            $(this).css({"background-color":"aqua","border-radius":"20%"});
        });
    });
    $("#e-mail").click(function() {
        $("#e-mail").focus(function() {
            $(this).css({"background-color":"aqua","border-radius":"20%"});
        });
    });
    $("#username").click(function() {
        $("#username").focus(function() {
            $(this).css({"background-color":"aqua","border-radius":"20%"});
        });
    });
    $("#password").click(function() {
        $("#password").focus(function() {
            $(this).css({"background-color":"aqua","border-radius":"20%"});
        });
    });
    $("#pass_check").click(function() {
        $("#pass_check").focus(function() {
            $(this).css({"background-color":"aqua","border-radius":"20%"});
        });
    });
    $("#btnsubmit").click(function() {
        $("#btnsubmit").focus(function() {
            $(this).css({"background-color":"aqua","border-radius":"20%"});
        });
        $("#btnsubmit").keypress();
        $("#btnsubmit").submit(function(){
            alert("Successfull to Register. Your form was submitted");
        });
        document).getElementById("label").innerHTML = "Logout";
        $("a").attr("href", "{% url 'register:logout_success' %}");
    });


    //Untuk login page
    $("#uname_login").click(function() {
        $("#uname_login").focus(function() {
            $(this).css({"background-color":"aqua","border-radius":"20%"});
        });
    });
    $("#pass_login").click(function() {
        $("#pass_login").focus(function() {
            $(this).css({"background-color":"aqua","border-radius":"20%"});
        });

    });


    //untuk ajax register
    var username_input = false;
    var email_input = false;
    $("#username").on("blur", function(){
        var username = $("#username").val();
        if (username == ""){
            username_input = false;
            return username_input;
        }
        $.ajax({
            url: "register.html",
            type: "post",
            data: {
                "username_check" : 1,
                "username" : username,
            },
            success: function(response){
                if (response == "taken"){
                    username_input = false;
                    $("#username").parent().removeClass();
                    $("#username").parent().addClass("form_error");
                    $('#username').siblings("span").text('Sorry. Username already taken');
                }
                else if (response == "not_taken"){
                    username_input = true;
                    $("#username").parent().removeClass();
                    $("#username").parent().addClass("form_success");
                    $("#username").siblings("span").text('Username available');
                }
            }
        });
    });

    $("#btnsubmit").on("click", function(){
        var username = $("#username").val();
        var password = $("#password").val();
        if (username_input == false){
            $('#error_msg').text('Fix the errors in the form first');
        }
        else{
            $.ajax({
                url: "register.html",
                type: "post",
                data: {
                    "save" : 1,
                    "username" : username,
                    "password" : password,
                },
                success: function(response){
                    alert('user saved');
      		        $('#username').val('');
      		        $('#password').val('');
                }
            });
        }
    });

});