from django.test import TestCase,Client
from django .http import HttpRequest
from .views import index, signin_page, login_func, login_success, logout_success
from django.urls import resolve
from .forms import RegisterForm, UserForm
from .models import Register

# Create your tests here.
class RegisterTest(TestCase):
    #Test untuk cek url dari register
    def test_url_address_register_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    #Test untuk cek fungsi index telah memiliki isi
    def test_index_is_written(self):
        self.assertIsNotNone(index)

    #Test untuk cek fungsi signin telah memiliki isi
    def test_signin_is_written(self):
        self.assertIsNotNone(signin_page)

    #Test untuk cek fungsi login_func telah memiliki isi
    def test_login_is_written(self):
        self.assertIsNotNone(login_func)

    #Test untuk cek fungsi login_success telah memiliki isi
    def test_login_success_is_written(self):
        self.assertIsNotNone(login_success)

    #Test untuk cek fungsi logout telah memiliki isi
    def test_logout_is_written(self):
        self.assertIsNotNone(logout_success)

    #Test untuk cek template html telah digunakan pada halaman register
    def test_template_html_index_is_exist(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'register.html')

    #Test untuk cek isi halaman register
    def test_content_html_is_exist(self):
        response = Client().get('/register/')
        target = resolve('/register/')
        self.assertContains(response,'username')
        self.assertTrue(target.func, index)

    def test_content_html_is_exist(self):
        response = Client().get('/register/')
        target = resolve('/register/')
        self.assertContains(response,'password')
        self.assertTrue(target.func, index)

    #Test untuk cek models dari app register
    def test_models_register(self):
        self.assertIsNotNone(Register)

    #Test untuk check form register
    def test_forms_register(self):
        self.assertIsNotNone(RegisterForm)

    def test_content_forms_register(self):
        response = Client().get('/register/')
        self.assertContains(response, 'first_name')
        self.assertContains(response, 'last_name')
        self.assertContains(response, 'password')
        self.assertContains(response, 'email')
        self.assertContains(response, 'password_check')

    def test_content_forms_login(self):
        self.assertIsNotNone(UserForm)
