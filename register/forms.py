from django import forms
from register.models import Register
from django.contrib.auth.models import User

class RegisterForm(forms.Form):
    first_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','type':'text'}))
    last_name = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control','type':'text'}))
    email = forms.EmailField(widget=forms.TextInput(attrs={'class':'form-control login-input'}), max_length=75, required=True)
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control login-input'}), min_length=5, max_length=30, required=True)
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control login-input'}), required=True)
    password_check = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control login-input'}), required=True)
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'username', 'password', 'password_check')

    # def clean(self):
    #     cleaned_data = super(RegisterForm, self).clean()
    #     first_name = cleaned_data.get('first_name')
    #     last_name = cleaned_data.get('last_name')
    #     email = cleaned_data.get('email')
    #     username = cleaned_data.get('username')
    #     password = cleaned_data.get('password')
    #     password_check = cleaned_data.get('confirmation_password')

    #     # Username
    #     if User.objects.filter(username=username).exists():
    #         msg = "Sorry, your username is exists"
    #         self.add_error('username', msg)

    #     # Password
    #     if password_check != password:
    #         msg = "Please make sure your password is the same"
    #         self.add_error('confirmation_password', msg)

    #     return cleaned_data

class UserForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={'class':'form-control'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'form-control'}))