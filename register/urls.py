from django.urls import path
from . import views

app_name = 'register'

urlpatterns = [

    path('', views.index, name='index'),
    path('signin/', views.signin_page, name='signin_page'),
    path('login_func/', views.login_func, name='login_func'),
    path('login_success/', views.login_success, name='login_success'),
    path('logout_success/', views.logout_success, name='logout_success'),
    # dilanjutkan ...
]
