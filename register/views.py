from django.shortcuts import render, HttpResponse, redirect
from django.urls import reverse
from register.forms import RegisterForm, UserForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from register.models import Register
from django.template.defaultfilters import slugify
from django.http import HttpResponseRedirect, HttpResponse
import datetime

# Create your views here.
def index(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            slug = slugify(username)

            user = User.objects.create_user(username, email, password, first_name = first_name, last_name = last_name)
            user.save()
            form_validated = True
            return redirect('register:login_success')
    else:
        form = RegisterForm()

    return render(request, 'register.html', locals())

def signin_page(request):
    return render(request,'login1.html')

def login_func(request):
    if request.method == 'POST':
        form = UserForm(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            login(request, user)
            request.session['my_session'] = username
            return HttpResponseRedirect(reverse('register:login_success'))
        else:
            gagal = "Please check again your username or password"
            return render(request,'login1.html', {'gagal':gagal})
    else:
        form = UserForm()

def login_success(request):
    if request.session.get('my_session') is not None:
        value = request.session['my_session']
    return render(request, 'homepage.html')

def logout_success(request):
    logout(request)
    return redirect('register:signin_page')
