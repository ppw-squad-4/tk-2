from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.models import User
from register.models import Register

# Register your models here.
class RegisterInLine(admin.StackedInline):
    model = Register
    can_delete = False
    verbose_name_plural = 'register'

class UserAdmin(BaseUserAdmin):
    inlines = (RegisterInLine,)

admin.site.unregister(User)    
admin.site.register(User, UserAdmin)