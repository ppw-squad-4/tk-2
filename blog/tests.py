from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .models import Comment

class BlogUnitTest (TestCase):
    def test_blog_url_exists(self):
        response = Client().get('/blog/')
        self.assertEqual(response.status_code, 200)
    def test_blog_html_template_used(self):
        response = Client().get('/blog/')
        self.assertTemplateUsed(response, 'blog.html')
    def test_using_blog_func(self):
        found = resolve('/blog/')
        self.assertEqual(found.func, views.blog)
    def test_models_working(self):
        commentation = Comment.objects.create(judul='Hans', comment='hi')
        count= Comment.objects.all().count()
        self.assertEqual(count,1)
