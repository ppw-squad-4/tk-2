from django import forms
from . import models

class Comment_Form(forms.Form):
    judul = forms.CharField(widget=forms.TextInput(attrs={'required' : 'True', 'class':'form-control',
                                'label' : 'judul comment', 'id':'judul'}))

    comment = forms.CharField(widget=forms.TextInput(attrs={'required' : 'True', 'class':'form-control',
                                'label' : 'comment', 'id':'comment'}))
class Meta:
    model = models.Comment
    fields = ('Comment')  