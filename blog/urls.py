from django.urls import path
from . import views

app_name = 'blog'

urlpatterns = [
    path('',views.blog, name='blog'),
    path('id1/',views.article1, name='article1'),
    path('id2/',views.article2, name='article2'),
    path('id3/',views.article3, name='article3'),
    path('response/',views.commentReturn, name="return_comment"),
    
] 
