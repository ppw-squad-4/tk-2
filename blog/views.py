import requests
from django.http import JsonResponse
from django.shortcuts import render
from . import models
from . import forms
from django.contrib.auth.models import User

# Create your views here.
def blog(request): 
    return render(request,'blog.html')
def article1(request):
    result = models.Comment.objects.all();
    form=forms.Comment_Form()
    return render(request,'article1.html',{"form":form,"comment":result})
def article2(request):
    result = models.Comment.objects.all();
    form=forms.Comment_Form()
    return render(request,'article2.html',{"form":form,"comment":result})   
def article3(request):
    result = models.Comment.objects.all();
    form=forms.Comment_Form()
    return render(request,'article3.html',{"form":form,"comment":result})

def commentReturn(request):
    if request.user.is_authenticated:
        if request.method == "POST":
            form = forms.Comment_Form(request.POST)
            if form.is_valid():
                nama = request.user.get_full_name()
                judul = request.POST['judul']
                comment = request.POST['comment']
                result = forms.Comment_Form.objects.create(nama=nama,
                                                             judul= form.data['judul'], 
                                                                comment = form.data['comment'])
                result.save()
                response = JsonResponse({'nama':nama, 'judul':judul,'comment':comment}, safe=False)
                return response
    
    pesan = 'Not Yet Login'
    response = JsonResponse({'status':'false','message':pesan})
    response.status_code = 403
    return response
    # def article1_response(request):
    # return render(request,'')
