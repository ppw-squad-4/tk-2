from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

class Comment(models.Model):
    nama = models.CharField(max_length=100,default="*Tanpa Nama*")
    judul = models.CharField(max_length=100,default="*Tanpa Judul*")
    published_date = models.DateTimeField(default=timezone.now)
    comment = models.CharField(max_length=1000,default="Artikel Bagus")