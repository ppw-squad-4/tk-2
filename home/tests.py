from django.test import TestCase, Client
from .views import index

class HomeTest(TestCase):
    def test_status_url_home_exits(self):
        response = Client().get('')
        self.assertEqual(response.status_code,200)

    def test_template_home_used(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'homepage.html')

    def test_index_is_written(self):
        self.assertIsNotNone(index)

# Create your tests here.
